# Compiler and flags
CC=gcc
LUAC=luac
PKG_PACKAGES=lua5.1
CFLAGS=-shared -pipe -Wall `pkg-config --cflags $(PKG_PACKAGES)` -fPIC
LDFLAGS=-shared `pkg-config --libs $(PKG_PACKAGES)` -ldoublefann

ifdef DEBUG
	CFLAGS+=-g -O0 -DIDEBUG
else
	CFLAGS+=-O2
endif

# Platform dependent
RM=rm -rf
CP=cp
MKDIR=mkdir -p
DESTDIR=/
PREFIX=/usr
NAME=lfann.so

# Rules

.PHONY: all
all: $(SOURCES) $(NAME)

$(NAME): src/interface.c src/data.c src/extension.c src/lfann.h src/net.c
	$(CC) $(CFLAGS) src/interface.c -o $@ $(LDFLAGS)

.PHONY: install
install: all
	$(MKDIR) $(DESTDIR)/$(PREFIX)/lib/lua/5.1
	$(CP) $(NAME) $(DESTDIR)/$(PREFIX)/lib/lua/5.1

.PHONY: clean
clean:
	$(RM) $(NAME)

.PHONY: help
help:
	@echo Options:
	@echo
	@echo DEBUG=1 to enable debug mode
