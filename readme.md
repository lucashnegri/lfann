lfann
=====

Info
----

lfann is a FANN 2.2 binding to Lua. It tries to follow Lua conventions,
such as counting from 1 (both neurons and layers have an offset of 1).
It also implements some new functions (isolated in the files extension.{c,h}).

There are packages for Ubuntu 13.10 on *ppa:lucashnegri/lua*.

Dependencies
------------

* FANN 2.2 with development files
* Lua 5.1.x, with development files

Install
-------

Edit the Makefile to suit your needs, and run

    $ make
    # make install [PREFIX=/usr/local]

Usage
-----

lfann follows the original C API. See _examples_ and _tests_.

Contact
-------

Lucas Hermann Negri - <lucashnegri@gmail.com>

http://oproj.tuxfamily.org
