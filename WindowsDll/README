There are various ways to use lfann with lua & fann for Windows. This method builds 
lfann.dll to work with lua51.dll (lua-5.1.4) & fanndouble.dll (fann-2.1.0) using
Visual Studio.

Some routines in lfann alloc memory which is then set free by fann. For that to work,
lfann.dll and fanndouble.dll have to be linked to use the same dynamically-loaded
C run-time. The default makefile included with fann-2.1.0 statically links in the
C run-time, so has to be modified to build fanndouble.dll.

The lfann.dll is loaded by lua [require"lfann"] using the lua load mechanisms; the
fanndouble.dll is loaded by Windows using its load mechanisms.


To build lfann.dll, first make a lua51.dll by running etc\luavs.bat from the top-level 
directory of the unpacked lua-5.1.4 source. That should also make lua51.lib, 
required along with the lua51 include files to make lfann.dll.

Second, make a fanndouble.dll by running [nmake "Release Double"] from the
MicrosoftWindowsDll sub-directory of the unpacked fann-2.1.0 source. That should also
make fanndouble.lib, required along with the fann include files to make lfann.dll. 
The makefile needs to be edited to build a fanndouble.dll that links dynamically
to the C run-time--see below.

Finally, make a lfann.dll by running lfannvs.bat from this sub-directory. The file
lfannvs.bat should be edited to be able to find the lua-5.1.4 and fann-2.1.0
directories.


Modifications to the makefile for fann in the MicrosoftWindowsDll sub-directory for
the "Release Double" section should be:

first, change /ML in the compile parameters to /MD

second, add in lines to run the manifest utility, mt, as below.

The section should then be something like:


# "Release Double" - fanndouble.dll

RDBLOBJ = "Release Double/fann.obj" "Release Double/fann_error.obj" "Release Double/fann_io.obj" "Release Double/fann_cascade.obj" "Release Double/fann_train.obj" "Release Double/fann_train_data.obj"

"Release Double Compile" :
    @echo Compiling Release Double...
    @if not exist "Release Double" mkdir "Release Double"
    @cl @<<
    /O2 /Ot /GL /I "../src/include" /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_USRDLL" /D "FANN_DLL_EXPORTS" /D "_WINDLL" /D "_MBCS" /FD /MD /Fo"Release Double/" /Fd"Release Double/vc70.pdb" /W3 /nologo /c /Zi /TC /FI "doublefann.h" $(SOURCE)
<<NOKEEP

"Release Double Link" : "Release Double Compile"
    @echo Linking Release Double...
    @link @<<
    /OUT:"Release Double/fanndouble.dll" /INCREMENTAL:NO /NOLOGO /DLL /DEBUG /PDB:"Release Double/fanndouble.pdb" /SUBSYSTEM:WINDOWS /OPT:REF /OPT:ICF /LTCG /IMPLIB:"Release Double/fanndouble.lib" /MACHINE:X86   kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib $(RDBLOBJ)
<<NOKEEP

"Release Double Manifest" : "Release Double Link"
    @echo Manifest Release Double...
    @if exist "Release Double\fanndouble.dll.manifest"^
       mt /nologo -manifest "Release Double\fanndouble.dll.manifest" -outputresource:"Release Double\fanndouble.dll";2

"Release Double" : "Release Double Manifest"
    @echo Copying Release Double to bin...
    @if not exist bin mkdir bin
    @copy "Release Double\fanndouble.dll" "bin\fanndouble.dll"
    @copy "Release Double\fanndouble.lib" "bin\fanndouble.lib"
